import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

agitation_start_temp = -3
MINUTES = 60
PAD_POWER = 100 #Watts
#root = '/home/einar/end_of_thaw/Bag thawer testing/28Sept Bag Thawer Test program/'
#root = '/home/einar/end_of_thaw/Bag thawer testing/Liquid nitrogen testing/'
#root = '/home/einar/end_of_thaw/Vial Runs/Vial runs 1/'
#root = '/home/einar/end_of_thaw/Vial Runs/Vial runs 2  TC vial/'
#root = '/home/einar/end_of_thaw/Vial Runs/Non TC runs/2mlVial 2ml fill runs/'
#root = '/home/einar/end_of_thaw/Vial Runs/Non TC runs/2ml Vial 1ml fill runs/'
#root = '/home/einar/end_of_thaw/Vial Runs/2ml empty vial/'
#root = '/home/einar/end_of_thaw/Vial Runs/Acetal 1 calibration/'
root = '/home/einar/end_of_thaw/Vial Runs/31-10-2016 Calibration runs/'
#root = '/home/einar/end_of_thaw/Vial Runs/Run to convice FDA cells dont die/'
#root = '/home/einar/end_of_thaw/Vial Runs/ForReport22Nov/'
def main():

	comparison_dict_IR0 = {}

	min_temp_time = {}
	estimated_end_of_thaw = {}
	agitation_start_time = {}
	dict_of_bag_types = getData(root, "data.csv")
	observed_end_of_thaw = getData(root, "manual data.csv")
	for bag in dict_of_bag_types:
		dict_of_bag_types[bag] = preprocess(dict_of_bag_types[bag])
		min_temp_time[bag] = {}
		estimated_end_of_thaw[bag] = {}
		agitation_start_time[bag] = {}
		for run in range (0, len(dict_of_bag_types[bag])):
			data = dict_of_bag_types[bag][run]
			data['Lowest IR'] = data.loc[:, ['IR0', 'IR1', 'IR2', 'IR3', 'IR4']].min(axis = 1)
			min_temp_point = data.iloc[data['Lowest IR'].idxmin()]

			comparison_dict_IR0[(bag + " run: " + str(run+1))] = min_temp_point.get_value('IR0', 0)
			
			min_temp_time[bag][run] = min_temp_point.get_value('Time', 0)
			#estimated_end_of_thaw[bag][run] = endOfThawAlgorithm(data, findFillVolume(bag), checkOverwrap(bag), min_temp_time[bag][run])
			#agitation_start_time[bag][run] = agitationTurnedOn(data, agitation_start_temp, min_temp_time[bag][run])
			dict_of_bag_types[bag][run] = data
	#EnergyInput(dict_of_bag_types)

	print "the number of runs is: ", len(comparison_dict_IR0)
	sorted_by_load = sorted(comparison_dict_IR0, key=comparison_dict_IR0.__getitem__)
	print sorted_by_load
	for key in sorted_by_load:
		print comparison_dict_IR0[key], key


	endOfThawEvaluator(estimated_end_of_thaw, observed_end_of_thaw)
	plotting(estimated_end_of_thaw, agitation_start_time, dict_of_bag_types)


def getData(root, filename):
	list_of_data_files = {}
	dict_of_bag_types = {}
	dir_list = os.walk(root).next()[1]
	for j in range (0, len(dir_list)):
		i = 0
		for subdir, dirs, files in os.walk(os.path.join(root, dir_list[j])):
			if os.path.exists(os.path.join(subdir, filename)):
				list_of_data_files[i] = pd.read_csv(os.path.abspath(os.path.join(subdir, filename)))
				i += 1
		dict_of_bag_types[dir_list[j]] = list_of_data_files
		list_of_data_files = {}
	return dict_of_bag_types


def preprocess(dict_of_data_files):
	for i in range(0, len(dict_of_data_files)):
		data_processing = dict_of_data_files[i]
		data_processing = data_processing.apply(pd.to_numeric, errors = 'coerce')
		if not 'Time' in data_processing.columns.values:
			data_processing.columns = [ 'Time', 'PV0', 'PV1', 'PV2', 'PV3', 'OP0', 'OP1', 'OP2', 'OP3',
			                            'P0', 'P1', 'P2', 'P3', 'I0', 'I1', 'I2', 'I3', 'D0', 'D1', 'FF0',
			                            'FF1', 'Power0', 'Power1', 'Power2', 'Power3', 'Current0', 'Current1', 'Current2', 'Current3',
			                            'IR0', 'IR1', 'IR2', 'IR3', 'IR4', 'SS0', 'estimate', 'TCO','TC1','TC2'
			                          ]
		index = pd.isnull(data_processing['Time']).nonzero()
		if np.any(index[0]):
			if index[0][-1] >= 100:
				data_processing = data_processing[0:index[0][0]]
		data_processing = data_processing.dropna(how = 'all')
		data_processing = data_processing.reset_index(drop = True)
		dict_of_data_files[i] = data_processing
	return dict_of_data_files


def findFillVolume(bag):
	return int(filter(str.isdigit, bag))


def checkOverwrap(bag):
	if "overwrap" in bag:
		return True
	else:
		return False


def agitationTurnedOn(data, agitation_start_temp, min_temp_time):
	agitation_is_on = data[((data['Time'] >= min_temp_time) & (data['Lowest IR'] >= agitation_start_temp))]
	if agitation_is_on.empty:
		agitation_start_time = {0:0}
	else:
		agitation_start_time = {agitation_is_on['Time'].iloc[0]:agitation_is_on['Lowest IR'].iloc[0]}
	return agitation_start_time


def endOfThawAlgorithm(data, fill_volume, overwrap, min_temp_time):
	if (fill_volume == 70 and overwrap == False):
		max_time_threshold = 4*MINUTES;
		max_time_threshold_2 = 7*MINUTES;
		max_IR_threshold = 30;
		max_IR_threshold_2 = 10;
	elif (fill_volume == 20 and overwrap == False):
		max_time_threshold = 4*MINUTES;
		max_time_threshold_2 = 7*MINUTES;
		max_IR_threshold = 30;
		max_IR_threshold_2 = 10;
	elif (fill_volume == 70 and overwrap == True):
		max_time_threshold = 5*MINUTES;
		max_time_threshold_2 = 8.5*MINUTES;
		max_IR_threshold = 30;
		max_IR_threshold_2 = 20;
	elif (fill_volume == 20 and overwrap == True):
		max_time_threshold = 4*MINUTES;
		max_time_threshold_2 = 7*MINUTES;
		max_IR_threshold = 30;
		max_IR_threshold_2 = 10;
	elif (fill_volume == 225 and overwrap == True):
		max_time_threshold = 5*MINUTES;
		max_time_threshold_2 = 9.5*MINUTES;
		max_IR_threshold = 30;
		max_IR_threshold_2 = 13;
	elif (fill_volume == 190 and overwrap == False):
		max_time_threshold = 5*MINUTES;
		max_time_threshold_2 = 9*MINUTES;
		max_IR_threshold = 30;
		max_IR_threshold_2 = 12;
	elif (fill_volume == 190 and overwrap == True):
		max_time_threshold = 5*MINUTES;
		max_time_threshold_2 = 9.5*MINUTES;
		max_IR_threshold = 30;
		max_IR_threshold_2 = 13;
	else:
		print "Error: fill_volume, overwrap combination not recognized"

	if (data['Time'].loc[data.shape[0]-1] > max_time_threshold_2):
		estimated_end_of_thaw = {max_time_threshold_2:data['Lowest IR'].loc[data.shape[0]-1]}
	else:
		end_of_thaw_has_been_reached = data[((data['Time'] >= min_temp_time) & (data['Lowest IR'] >= max_IR_threshold_2))]
		if end_of_thaw_has_been_reached.empty:
			estimated_end_of_thaw = {data['Time'].loc[data.shape[0]-1]:data['Lowest IR'].loc[data.shape[0]-1]}
		else:
			estimated_end_of_thaw = {end_of_thaw_has_been_reached['Time'].iloc[0]:end_of_thaw_has_been_reached['Lowest IR'].iloc[0]}
	return estimated_end_of_thaw


def EnergyInput(dict_of_bag_types):
	bag_to_energy_dict = {}
	for bag in dict_of_bag_types:
		energy = pd.DataFrame(columns = ['Pad 0','Pad 1','Pad 2','Pad 3'], index=range(0,len(dict_of_bag_types[bag])))
		bag_to_energy_dict[bag] = energy
		for run in dict_of_bag_types[bag]:
			data = dict_of_bag_types[bag][run]
			for i in range(0,4):
				energy['Pad ' + str(i)][run] = data['Time'].iloc[0]*data['OP' + str(i)].iloc[0]
				for t in range(1, len(data['Time'])):
					energy['Pad ' + str(i)][run] = (data['Time'].iloc[t] - data['Time'].iloc[t-1])*data['OP' + str(i)].iloc[t] + energy['Pad ' + str(i)][run]
				if i == 3:
					energy['Pad ' + str(i)][run] = energy['Pad ' + str(i)][run]*3
		print bag
		print energy*PAD_POWER/(1000*100)
		print "Energy expended through whole run for: " + bag
		energy_mean = energy.mean(axis = 0)*PAD_POWER/(1000*100)
		print  energy_mean," kJ"
		print "Total Energy", sum(energy_mean)

def endOfThawEvaluator(estimated_end_of_thaw, observed_end_of_thaw):
	estimated = pd.DataFrame(estimated_end_of_thaw)
	for bag in estimated_end_of_thaw:
		for run in range(0, len(estimated_end_of_thaw[bag])):
			temp = estimated_end_of_thaw[bag][run]
			temp = temp.keys()
			temp = temp[0]
			estimated[bag][run] = temp

	observed = pd.DataFrame(estimated_end_of_thaw)
	observed = pd.DataFrame(data=None, columns=observed.columns,index=observed.index)
	for bag in estimated_end_of_thaw:
		for run in range(0, len(estimated_end_of_thaw[bag])):
			if observed_end_of_thaw[bag]:
				temp2 = observed_end_of_thaw[bag][run]
				temp2 = temp2['Observed_EoT'].loc[0]
				observed[bag][run] = temp2

	differences = observed.subtract(estimated, fill_value = np.NaN)
	print "Standard deviation of differences is: ", differences.stack().std()
	print "Mean difference is: ", differences.stack().mean()
	print "Greatest difference is: ", differences.stack().max()
	print "Minimum difference is: ", differences.stack().min()


def xShift(dict_of_bag_types, bag, run):
	data = dict_of_bag_types[bag][run]
	shift = data.iloc[data['Lowest IR'].idxmin()]*(-1)
	shift = shift.get_value('Time', 0)
	return shift


def calibration(IR, bag, IR_sensor_number):
	IR = IR.values.tolist()
	print IR
	rho = []
	if IR_sensor_number == 'IR0':
		if bag.find('TCK100005'):
			rho = [-2.8310e-03, 1.3556e+00, -2.4862e+01]
		elif bag.find('TCK100004'):
			rho = [-3.4002e-03, 1.4024e+00, -2.4429e+01]
		elif bag.find('TCK100003'):
			rho = [-3.4407e-03, 1.3572e+00, -2.5501e+01]
		elif bag.find('TCK100002'):
			rho = [-4.5585e-03, 1.5846e+00, -3.1896e+01]
		elif bag.find('TCK000005'):
			rho = [-6.8028e-03, 1.8655e+00, -4.3025e+01]
	if IR_sensor_number == 'IR1':
	 	if bag.find('TCK100005'):
	 		rho = [4.8546e-04, 1.5847e+00, -5.1649e+01]
		elif bag.find('TCK100004'):
			rho = [1.5196e-03, 1.5249e+00, -5.0035e+01]
		elif bag.find('TCK100003'):
			rho = [5.1921e-03, 1.7430e+00, -7.3906e+01]
		elif bag.find('TCK100002'):
			rho = [3.8646e-03, 1.7208e+00, -6.5493e+01]
		elif bag.find('TCK000005'):
			rho = [5.6369e-03, 1.7887e+00, -7.7458e+01]
	print rho
	calibrated_values = [x*x*rho[0]+x*rho[1]+rho[2] for x in IR]
	return(calibrated_values)

def subtractLists(IR_values, TC_values):
	difference = [a - b for a, b in zip(IR_values, TC_values)]
	return difference


def plotting(estimated_end_of_thaw, agitation_start_time, dict_of_bag_types):
	for bag in dict_of_bag_types:
		plt.figure(bag)

		#shift = xShift(dict_of_bag_types, 'Baseline', 0)
		#x_values = dict_of_bag_types['Baseline'][0]['Time'].values.tolist()
		#x_values = [x+shift for x in x_values]

		#TC_values_1 = dict_of_bag_types['Baseline'][0]['TC1'].values.tolist()
		#plt.plot(x_values, TC_values_1, '-o', c='red', label = "TC1 Baseline")

		#TC_values_2 = dict_of_bag_types['Baseline'][0]['TC2'].values.tolist()
		#plt.plot(x_values, TC_values_2, '-o',c='blue', label = "TC2 Baseline")


		for run in range(0, len(dict_of_bag_types[bag])):
			shift = xShift(dict_of_bag_types, bag, run)
			x_values = dict_of_bag_types[bag][run]['Time'].values.tolist()
			x_values = [x+shift for x in x_values]

			IR0_values = dict_of_bag_types[bag][run]['IR0'].values.tolist()
			plt.plot(x_values, IR0_values, label = "IR0 run " + str(run + 1))
			#if run == 1:
			#	plt.plot(x_values, IR0_values, label = "IR0 TCK100008")
			#else:
			#	plt.plot(x_values, IR0_values, label = "IR0 TCK100011")

			#IR1_values = dict_of_bag_types[bag][run]['IR1'].values.tolist()
			#plt.plot(x_values, IR1_values, '--', label = "IR1 run " + str(run + 1))

			TC_values_1 = dict_of_bag_types[bag][run]['TC1'].values.tolist()
			plt.plot(x_values, TC_values_1, '-o', label = "TC1 run " + str(run + 1))
			#if run == 1:
			#	plt.plot(x_values, TC_values_1, label = "TC1 TCK100008")
			#else:
			#	plt.plot(x_values, TC_values_1, label = "TC1 TCK100011")

			#TC_values_2 = dict_of_bag_types[bag][run]['TC2'].values.tolist()
			#lt.plot(x_values, TC_values_2, '-o',c='green', label = "TC2 run " + str(run + 1))

			#difference_1 = subtractLists(IR0_values, TC_values_1)
			#plt.plot(x_values, difference_1, '-o', label = "TC1 and IR0 difference run " + str(run + 1))

			##difference_2 = subtractLists(IR1_values, TC_values_2)
			##plt.plot(x_values, difference_2, '-o', label = "TC2 and IR1 difference run " + str(run + 1))			


			calibrated_values = calibration(dict_of_bag_types[bag][run]['IR0'],bag, 'IR0')
			plt.plot(x_values, calibrated_values, '_', label = "Calibrated IRO run " + str(run + 1))
			
			#calibrated_values = calibration(dict_of_bag_types[bag][run]['IR1'],bag, 'IR1')
			#plt.plot(x_values, calibrated_values, 'x', label = "Calibrated IR1 run " + str(run + 1))
			
			#for key in estimated_end_of_thaw[bag][run]:
				#plt.plot(key+shift, estimated_end_of_thaw[bag][run][key], 'bx')
			#for key in agitation_start_time[bag][run]:
				#if agitation_start_time[bag][run] != False:
					#plt.plot(key+shift, agitation_start_time[bag][run][key], 'o')
		plt.legend(loc='lower right', shadow=True)
		plt.ylabel('Degrees Celsius', fontsize=18)
		plt.xlabel('Seconds', fontsize=18)
		plt.title(bag, fontsize=18)

	plt.show()


if __name__ == '__main__':
	main()
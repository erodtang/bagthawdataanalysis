import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pprint import pprint
import re


target_internal_temp = 8
root = '/home/einar/Testing/AverageOf2/analyse'
#root = '/home/einar/Testing/AverageOf2/Data folder/Miltenyi 500 with 100ml fill overwrap'
algorithm_success_total = {}

def main():
	#A dictionary of lists. Each list is filled with dataframes. 
	#Every entry in a list being a different run. Every entry in the dict being a different bag
	dict_of_run_types_data = getData(root, "data.csv") 
	dict_of_run_types_manual = getData(root, "manual data.csv")
	for run_type in dict_of_run_types_data:
		plotBag(run_type)
		algorithm_success_run_type = []
		for run in dict_of_run_types_data[run_type]:
			#Extract data frames
			data_frame = dict_of_run_types_data[run_type][run]
			observed_eot = dict_of_run_types_manual[run_type][run]

			preprocess(data_frame)
			lowest_IR(data_frame)

			eot_prediction = algorithm(data_frame)
			algorithm_success_run_type.extend( [algorithmEvaluator(observed_eot, eot_prediction, target_internal_temp)] )

			plotRun(data_frame)
			plotPrediction(eot_prediction)
		algorithm_success_total.update({run_type: float(sum(algorithm_success_run_type))/(len(dict_of_run_types_data[run_type]))})
		
	pprint(algorithm_success_total)


#Algorithms
##################################
def algorithm2(data_frame):
	eot_prediction = data_frame['Time'].loc[data_frame.shape[0]-1]
	return eot_prediction

def algorithm(data_frame):
	IR_threshold = 0
	lowest_IR_row_index = data_frame.iloc[data_frame['Lowest IR'].idxmin()].name#Get index of row with the lowest IR reading
	data_frame = data_frame.loc[lowest_IR_row_index:]#Slice dataframe leaving from lowest IR point til end
	eot_prediction = data_frame[data_frame['Lowest IR'] > IR_threshold].iloc[0]['Time']##Apply end of thaw condition
	print eot_prediction
	return eot_prediction

def algorithm3(data_frame):
	return eot_prediction
##################################

#Averaging
##################################
def lowest_IR(data_frame):
	data_frame['Lowest IR'] = data_frame.loc[:, ['IR0', 'IR1', 'IR2', 'IR3', 'IR4']].min(axis = 1)
	return data_frame
##################################

def getData(root, filename):
	list_of_data_files = {}
	dict_of_run_types = {}
	dir_list = os.walk(root).next()[1]
	for j in range (0, len(dir_list)):
		for subdir, dirs, files in os.walk(os.path.join(root, dir_list[j])):
			if os.path.exists(os.path.join(subdir, filename)):
				i = re.search('Run (.+?)',subdir)
				if i:
					runNumber = i.group(1)
				list_of_data_files[runNumber] = pd.read_csv(os.path.abspath(os.path.join(subdir, filename)))
		dict_of_run_types[dir_list[j]] = list_of_data_files
	return dict_of_run_types

def preprocess(data_frame):
	data_frame = data_frame.apply(pd.to_numeric, errors = 'coerce')
	if not 'Time' in data_frame.columns.values:
		data_frame.columns = [ 'Time', 'PV0', 'PV1', 'PV2', 'PV3', 'OP0', 'OP1', 'OP2', 'OP3',
		                            'P0', 'P1', 'P2', 'P3', 'I0', 'I1', 'I2', 'I3', 'D0', 'D1', 'FF0',
		                            'FF1', 'Power0', 'Power1', 'Power2', 'Power3', 'Current0', 'Current1', 'Current2', 'Current3',
		                            'IR0', 'IR1', 'IR2', 'IR3', 'IR4', 'SS0', 'estimate'
		                          ]
	index = pd.isnull(data_frame['Time']).nonzero()
	if np.any(index[0]):
		if index[0][-1] >= 100:
			data_frame = data_frame[0:index[0][0]]
	data_frame = data_frame.dropna(how = 'all')
	data_frame = data_frame.reset_index(drop = True)
	return data_frame

def algorithmEvaluator(observed_eot, eot_prediction, target_internal_temp):
	if (observed_eot.iloc[0]['Time'] > eot_prediction) != (observed_eot.iloc[0]['Internal temp'] > target_internal_temp):
		betterRun = True
	else:
		betterRun = False
	return betterRun


#Plotting
def plotBag(run_type):
	plt.figure(run_type)

def plotRun(data_frame):
	return

def plotPrediction(eot_prediction):
	return

def xShift(data_frame):
	shift = data_frame.iloc[data_frame['Lowest IR'].idxmin()]*(-1)
	shift = shift.get_value('Time', 0)
	return shift

if __name__ == '__main__':
	main()
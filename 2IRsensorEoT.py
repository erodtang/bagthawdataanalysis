import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import re

agitation_start_temp = -15
MINUTES = 60
PAD_POWER = 100 #Watts
target_internal_temp = 6

root = '/home/einar/Testing/AverageOf2/Data folder'

def main():
	min_temp_time = {}
	estimated_end_of_thaw = {}
	agitation_start_time = {}
	dict_of_bag_types = getData(root, "data.csv")
	observed_end_of_thaw = getData(root, "manual data.csv")
	for bag in dict_of_bag_types:
		dict_of_bag_types[bag] = preprocess(dict_of_bag_types[bag])
		min_temp_time[bag] = {}
		estimated_end_of_thaw[bag] = {}
		agitation_start_time[bag] = {}
		for run in dict_of_bag_types[bag]:
			data = dict_of_bag_types[bag][run]
			
			IRdata = data.loc[:,['IR0', 'IR1', 'IR2', 'IR3', 'IR4']]
			IRDataSorted = pd.DataFrame(np.sort(IRdata.values) [:,-5:], columns=[ '5th-largest','4th-largest', '3rd-largest','2nd-largest','largest'])

			data['Lowest IR'] = IRDataSorted['5th-largest']
			data['5th4thAverage'] = IRDataSorted.loc[:,['5th-largest', '4th-largest']].mean(axis = 1)
			data['AverageAll'] = IRDataSorted.loc[:,['5th-largest','4th-largest', '3rd-largest', '2nd-largest','largest']].mean(axis = 1)

			min_temp_point = data.iloc[data['Lowest IR'].idxmin()]
			min_temp_time[bag][run] = min_temp_point.get_value('Time', 0)
			#estimated_end_of_thaw[bag][run] = endOfThawAlgorithm(data, findFillVolume(bag), checkOverwrap(bag), min_temp_time[bag][run])
			agitation_start_time[bag][run] = agitationTurnedOn(data, agitation_start_temp, min_temp_time[bag][run])
			dict_of_bag_types[bag][run] = data
	EnergyInput(dict_of_bag_types)
	endOfThawEvaluator(estimated_end_of_thaw, observed_end_of_thaw)
	plotting(estimated_end_of_thaw, agitation_start_time, dict_of_bag_types)


def getData(root, filename):
	list_of_data_files = {}
	dict_of_bag_types = {}
	dir_list = os.walk(root).next()[1]
	for j in range (0, len(dir_list)):
		
		for subdir, dirs, files in os.walk(os.path.join(root, dir_list[j])):
			if os.path.exists(os.path.join(subdir, filename)):
				i = re.search('Run (.+?)',subdir)
				if i:
					runNumber = i.group(1)
				list_of_data_files[runNumber] = pd.read_csv(os.path.abspath(os.path.join(subdir, filename)))
		dict_of_bag_types[dir_list[j]] = list_of_data_files
	return dict_of_bag_types


def preprocess(dict_of_data_files):
	#for i in range(0, len(dict_of_data_files)):
	for key in dict_of_data_files:
		data_processing = dict_of_data_files[key]
		data_processing = data_processing.apply(pd.to_numeric, errors = 'coerce')
		if not 'Time' in data_processing.columns.values:
			data_processing.columns = [ 'Time', 'PV0', 'PV1', 'PV2', 'PV3', 'OP0', 'OP1', 'OP2', 'OP3',
			                            'P0', 'P1', 'P2', 'P3', 'I0', 'I1', 'I2', 'I3', 'D0', 'D1', 'FF0',
			                            'FF1', 'Power0', 'Power1', 'Power2', 'Power3', 'Current0', 'Current1', 'Current2', 'Current3',
			                            'IR0', 'IR1', 'IR2', 'IR3', 'IR4', 'SS0', 'estimate'
			                          ]
		index = pd.isnull(data_processing['Time']).nonzero()
		if np.any(index[0]):
			if index[0][-1] >= 100:
				data_processing = data_processing[0:index[0][0]]
		data_processing = data_processing.dropna(how = 'all')
		data_processing = data_processing.reset_index(drop = True)
		dict_of_data_files[key] = data_processing
	return dict_of_data_files


def findFillVolume(bag):
	return int(filter(str.isdigit, bag))


def EnergyInput(dict_of_bag_types):
	bag_to_energy_dict = {}
	for bag in dict_of_bag_types:
		bag_to_energy_dict[bag] = pd.DataFrame(0, columns = ['Pad 0','Pad 1','Pad 2','Pad 3'], index=range(0,len(dict_of_bag_types[bag])))
		run_number = 0
		for run in dict_of_bag_types[bag]:
			run_number = run_number + 1;
			data = dict_of_bag_types[bag][run]
			for i in range(0,4):
				temp = data['Time'].iloc[0]*data['OP' + str(i)].iloc[0]
				bag_to_energy_dict[bag].set_value(run_number,'Pad ' + str(i), temp.item())
				print "After"
				print bag_to_energy_dict[bag]
				for t in range(1, len(data['Time'])):
					temp = (data['Time'].iloc[t] - data['Time'].iloc[t-1])*data['OP' + str(i)].iloc[t] + bag_to_energy_dict[bag].get_value(run_number, 'Pad ' + str(i))
					bag_to_energy_dict[bag].set_value(run_number,'Pad ' + str(i), temp.item())
				if i == 3:
					temp = bag_to_energy_dict[bag].get_value(run_number,'Pad ' + str(i))*3
					bag_to_energy_dict[bag].set_value(run_number, 'Pad ' + str(i), temp.item())
		print bag_to_energy_dict[bag]
		print bag
		print bag_to_energy_dict[bag]*PAD_POWER*0.00001#/(1000*100)
		print "Energy expended through whole run for: " + bag
		energy_mean = bag_to_energy_dict[bag].mean(axis = 0)*PAD_POWER*0.00001#/(1000*100)
		print  energy_mean, " kJ"
		print "Total Energy", sum(energy_mean)
'''
def EnergyInput(dict_of_bag_types):
	bag_to_energy_dict = {}
	for bag in dict_of_bag_types:
		energy = pd.DataFrame(columns = ['Pad 0','Pad 1','Pad 2','Pad 3'], index=range(0,len(dict_of_bag_types[bag])))
		bag_to_energy_dict[bag] = energy
		for run in dict_of_bag_types[bag]:
			data = dict_of_bag_types[bag][run]
			for i in range(0,4):
				energy[run]['Pad ' + str(i)] = data['Time'].iloc[0]*data['OP' + str(i)].iloc[0]
				for t in range(1, len(data['Time'])):
					energy[run]['Pad ' + str(i)] = (data['Time'].iloc[t] - data['Time'].iloc[t-1])*data['OP' + str(i)].iloc[t] + energy['Pad ' + str(i)][run]
				if i == 3:
					energy[run]['Pad ' + str(i)] = energy[run]['Pad ' + str(i)]*3
		print bag
		print energy*PAD_POWER/(1000*100)
		print "Energy expended through whole run for: " + bag
		energy_mean = energy.mean(axis = 0)*PAD_POWER/(1000*100)
		print  energy_mean," kJ"
		print "Total Energy", sum(energy_mean)
'''
def checkOverwrap(bag):
	if "overwrap" in bag:
		return True
	else:
		return False


def agitationTurnedOn(data, agitation_start_temp, min_temp_time):
	agitation_is_on = data[((data['Time'] >= min_temp_time) & (data['Lowest IR'] >= agitation_start_temp))]
	if agitation_is_on.empty:
		agitation_start_time = {0:0}
	else:
		agitation_start_time = {agitation_is_on['Time'].iloc[0]:agitation_is_on['Lowest IR'].iloc[0]}
	return agitation_start_time


def endOfThawAlgorithm(data, fill_volume, overwrap, min_temp_time):

	max_time_threshold = 4*MINUTES;
	max_time_threshold_2 = 7*MINUTES;
	max_IR_threshold = 30;
	max_IR_threshold_2 = 10;

	if (data['Time'].loc[data.shape[0]-1] > max_time_threshold_2):
		estimated_end_of_thaw = {max_time_threshold_2:data['Lowest IR'].loc[data.shape[0]-1]}
	else:
		end_of_thaw_has_been_reached = data[((data['Time'] >= min_temp_time) & (data['Lowest IR'] >= max_IR_threshold_2))]
		if end_of_thaw_has_been_reached.empty:
			estimated_end_of_thaw = {data['Time'].loc[data.shape[0]-1]:data['Lowest IR'].loc[data.shape[0]-1]}
		else:
			estimated_end_of_thaw = {end_of_thaw_has_been_reached['Time'].iloc[0]:end_of_thaw_has_been_reached['Lowest IR'].iloc[0]}
	return estimated_end_of_thaw

def endOfThawEvaluator(estimated_end_of_thaw, observed_end_of_thaw):
	estimated = pd.DataFrame(estimated_end_of_thaw)
	for bag in estimated_end_of_thaw:
		for run in range(0, len(estimated_end_of_thaw[bag])):
			temp = estimated_end_of_thaw[bag][run]
			temp = temp.keys()
			temp = temp[0]
			estimated[bag][run] = temp

	observed = pd.DataFrame(estimated_end_of_thaw)
	observed = pd.DataFrame(data=None, columns=observed.columns,index=observed.index)
	for bag in estimated_end_of_thaw:
		for run in range(0, len(estimated_end_of_thaw[bag])):
			if observed_end_of_thaw[bag]:
				temp2 = observed_end_of_thaw[bag][run]
				temp2 = temp2['Observed_EoT'].loc[0]
				observed[bag][run] = temp2

	differences = observed.subtract(estimated, fill_value = np.NaN)
	print "Standard deviation of differences is: ", differences.stack().std()
	print "Mean difference is: ", differences.stack().mean()
	print "Greatest difference is: ", differences.stack().max()
	print "Minimum difference is: ", differences.stack().min()

#def discreteEndOfThawEvaluator(estimated_end_of_thaw, observed_end_of_thaw):
#	num_improved = 0;
#	for bag in estimated_end_of_thaw :
#		for run in range(0, len(estimated_end_of_thaw[bag]))
#		num_improved += 1
#	
#	print "Total number of improved runs:", num_improved


def xShift(dict_of_bag_types, bag, run):
	data = dict_of_bag_types[bag][run]
	shift = data.iloc[data['Lowest IR'].idxmin()]*(-1)
	shift = shift.get_value('Time', 0)
	return shift


def plotting(estimated_end_of_thaw, agitation_start_time, dict_of_bag_types):
	for bag in dict_of_bag_types:
		plt.figure(bag)
		for run in dict_of_bag_types[bag]:
			shift = xShift(dict_of_bag_types, bag, run)
			x_values = dict_of_bag_types[bag][run]['Time'].values.tolist()
			x_values = [x+shift for x in x_values]
			y_values = dict_of_bag_types[bag][run]['Lowest IR'].values.tolist()
			x_filt = x_values[x_values > 10]
			plt.plot(x_values, y_values, label = 'Run ' + run)
			#for key in estimated_end_of_thaw[bag][run]:
			#	plt.plot(key+shift, estimated_end_of_thaw[bag][run][key], 'bx')
			for key in agitation_start_time[bag][run]:
				if agitation_start_time[bag][run] != False:
					plt.plot(key+shift, agitation_start_time[bag][run][key], 'o')
		plt.legend(loc='lower right', shadow=True)

		plt.figure(bag + ' average')
		for run in dict_of_bag_types[bag]:
			shift = xShift(dict_of_bag_types, bag, run)
			x_values = dict_of_bag_types[bag][run]['Time'].values.tolist()
			x_values = [x+shift for x in x_values]
			#y_values = dict_of_bag_types[bag][run]['5th4thAverage'].values.tolist()
			y_values = dict_of_bag_types[bag][run]['AverageAll'].values.tolist()
			x_filt = x_values[x_values > 10]
			plt.plot(x_values, y_values, label = 'Run ' + run)
			#for key in estimated_end_of_thaw[bag][run]:
			#	plt.plot(key+shift, estimated_end_of_thaw[bag][run][key], 'bx')
			for key in agitation_start_time[bag][run]:
				if agitation_start_time[bag][run] != False:
					plt.plot(key+shift, agitation_start_time[bag][run][key], 'o')
		plt.legend(loc='lower right', shadow=True)

	plt.show()


if __name__ == '__main__':
	main()